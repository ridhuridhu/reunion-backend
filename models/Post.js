const mongoose = require('mongoose');
const postSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.ObjectId,
        ref: "User"
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    likes: [{
        type: mongoose.Schema.ObjectId,
        ref: "User"
    }],
    comments: [{
        text: {
            type: String
        },
        user: {
            type: mongoose.Schema.ObjectId,
            ref: "User"
        }
    }],
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt:false
    }
});

module.exports = mongoose.model('Post', postSchema);