const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    password: String,
    followers: [{
        type: mongoose.Schema.ObjectId,
        ref: "User"
    }],
    following: [{
        type: mongoose.Schema.ObjectId,
        ref: "User"
    }],
    posts: [{
        type: mongoose.Schema.ObjectId,
        ref: "Post"
    }],
});

module.exports = mongoose.model("User", userSchema);