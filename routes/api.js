const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const Post = require("../models/Post");

const verfiyToken = (req, res, next) => { //Auth middleware
    const token = req.body.token || req.query.token || req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send("A token is required for authentication");
    }
    try {
        const decoded = jwt.verify(token, "jwt_pass_word");
        req.user = decoded;
    } catch (err) {
        console.log(err);
        return res.status(401).send("Invalid Token");
    }
    return next();
};

router.get("/", (req, res) => {
    return res.json({
        success: true,
        data: "api is working"
    });
});

router.post("/authenticate", async (req, res) => {
    try {
        let {
            email,
            password
        } = req.body;
        let user = await User.findOne({
            email: email
        });
        if (!user) return res.json({
            data: "no such user found!"
        });
        let token = jwt.sign({
            id: user._id
        }, "jwt_pass_word", {
            expiresIn: '5d'
        });

        return res.header('auth', token).json({
            success: true,
            token
        });

    } catch (error) {
        return res.status(400).json({
            success: false,
            data: "Error in signin"
        });
    }

});

router.post("/follow/:id", verfiyToken, async (req, res) => {
    try {
        let user = await User.findById(req.params.id);
        if (!user) {
            return res.json({
                success: false,
                data: "No such user found"
            });
        }

        if (user._id === req.user.id) {
            return res.json({
                success: false,
                data: "error in updating"
            });
        }

        await User.findByIdAndUpdate(req.params.id, {
            $addToSet: {
                followers: req.user.id
            }
        });
        await User.findByIdAndUpdate(req.user.id, {
            $addToSet: {
                following: req.params.id
            }
        });

        return res.json({
            success: true
        });
    } catch (error) {
        return res.status(400).json({
            success: false,
            data: "Error in updating"
        });
    }
});

router.post("/unfollow/:id", verfiyToken, async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            return res.json({
                success: false,
                data: "No such user found"
            });
        }

        if (user._id === req.user.id) {
            return res.json({
                success: false,
                data: "you can't follow/unfollow yourself"
            });
        }

        await User.findByIdAndUpdate(req.params.id, {
            $pull: {
                followers: req.user.id
            }
        });
        await User.findByIdAndUpdate(req.user.id, {
            $pull: {
                following: req.params.id
            }
        });

        return res.json({
            success: true
        });

    } catch (error) {
        return res.status(400).json({
            success: false,
            data: "Error in updating"
        });
    }
});

router.get("/user", verfiyToken, async (req, res) => { 
    try {
        let user = await User.findById(req.user.id);
        //console.log(user.followers.length);
        user.noOfFollowers = user.followers.length;
        user.noOfFollowing = user.following.length;
        return res.json({
            "username": user.username,
            "followers": user.noOfFollowers || 0,
            "following": user.OfFollowing || 0
        });

    } catch (error) {
        return res.status(400).json({
            success: false,
            data: "Error in get user data"
        });
    }

});

router.post("/posts", verfiyToken, async (req, res) => { 
    try {
        let {
            title,
            description
        } = req.body;
        let post = await Post.create({
            title,
            description,
            user: req.user.id
        });
        await User.findByIdAndUpdate(req.user.id, {
            $push: {
                posts: post._id
            }
        });
        return res.json({
            success:true,
            "post_id": post._id,
            "title": post.title,
            "description": post.description,
            "created_at": post.created_at
        });
        
    } catch (error) {
        return res.status(400).json({
            success: false,
            data: "Error posting"
        });
    }
});

router.delete("/posts/:id", verfiyToken, async (req, res) => {  
    try {
        await User.findByIdAndUpdate(req.user.id, {
            $pull: {
                posts: req.params.id
            }
        });
        await Post.findByIdAndDelete(req.params.id);
        return res.json({
            success: true,
            data: "Post deleted"
        });

    } catch (error) {
        return res.json({
            success: false,
            data: "post not found"
        });
    }
});

router.get("/posts/:id", async (req, res) => { 

    try {
        let post = await Post.findById(req.params.id).populate("user");
        post.likesCount = (post.likes).length || 0;
        post.commentsCount = post.comments.length || 0;

        return res.json({
            success: true,
            data: post
        });
    } catch (error) {
        return res.json({
            success: false,
            data: "post not found"
        });
    }

});

router.get("/all_posts", verfiyToken, async (req, res) => { 

    try {
        let posts = await User.findById(req.user.id).populate('posts').select('_id title');

        return res.json({
            success: true,
            posts
        });

    } catch (error) {
        return res.json({
            success: false,
            data: "post not found"
        });
    }

});

router.post("/like/:id", verfiyToken, async (req, res) => { 
    try {
        let post = await Post.findByIdAndUpdate(req.params.id, {
            $addToSet: {
                likes: req.user.id
            }
        });
        return res.status(200).json({
            success: true,
            post
        });
    } catch (error) {
        return res.status(400).json({
            success: false,
            data: "Error in updating"
        });
    }


});

router.post("/unlike/:id ", verfiyToken, async (req, res) => { 
    try {
        await Post.findByIdAndUpdate(req.user.id, {
            $pull: {
                likes: req.user.id
            }
        });

        return res.status(200).json({
            success: true
        });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            success: false,
            data: "Error in updating"
        });
    }

});

router.post("/comment/:id ", verfiyToken, async (req, res) => {
    try {
        let post = await Post.findByIdAndUpdate(req.params.id, {
            $push: {
                comments: {
                    text: req.body.comment,
                    user: req.user.id
                }
            }
        });

        return res.json({
            success: true,
            data: post
        });
    } catch (error) {
        return res.status(400).json({
            success: false,
            data: "Error in updating"
        });
    }
});


module.exports = router;