require('dotenv').config();
const express = require("express");
const apiRoutes = require("./routes/api");
const app = express();
const port = process.env.PORT || 3000;
const mongoose = require("mongoose");
const uri = process.env.MONGO_URL;
//const morgan = require('morgan');
//app.use(morgan('dev'));
app.use(express.json());
////morgan('tiny');

app.get("/", (req, res) => {
  res.redirect("/api");
});
app.use("/api", apiRoutes);

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection
  .once('open', () => console.log('dataBase connected!'))
  .on('error', err => console.error(err));

const server = app.listen(port, () => {
  console.log(`server running on ${port}`);
});